const router = require('express').Router()

router.use((request, response, next) => {
  next()
})

router.get('/', (request, response) => {
  require('./controller').index(request, response)
})

router.get('/test', (request, response) => {
  require('./controller').test(request, response)
})

module.exports = router
