/**
 * Module dependencies.
 */
const chalk = require('chalk');
const dotenv = require('dotenv').config();

const express = require('express');
const session = require('express-session');
const uuid = require('uuid/v4')
const cookieParser = require('cookie-parser')

const MySQLStore = require('express-mysql-session')(session);

const passport = require('passport'),
  passport_localStrategy = require('passport-local').Strategy,
  passport_oauthStrategy = require('passport-oauth').Strategy;
const bcrypt = require('bcrypt')


/**
 * load environmen
 */
if (dotenv.error) {
  console.log(dotenv.error);
}

/**
 * init
 */
const app = express();
app.set('host', process.env.DATEPOLL_BASE);
app.set('port', process.env.DATEPOLL_PORT);

var connection = require('./database').getDatabaseConnection();
app.use(session({
  store: new MySQLStore({
    endConnectionOnClose: true,
    charset: 'utf8mb4_bin',
    schema: {
      tableName: 'sessions',
      columnNames: {
        session_id: 'session_id',
        expires: 'expires',
        data: 'data'
      }
    }
  }, connection),
  secret: 'dsaklfjöaslkdfjopqiweurpqowiernc,mxyn.v234',
  resave: false,
  saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());
app.use(cookieParser())

/** 
 * initialize passport
 */
passport.use(new passport_localStrategy(
  (user, password, done) => {
    findUser(username, (err, user) => {
      con.query("SELECT * FROM users WHERE email = ?", [user], (err, result) => {
        if (err) {
          return done(err)
        }

        // User not found
        if (!user) {
          return done(null, false)
        }

        bcrypt.compare(password, user.passwordHash, (err, isValid) => {
          if (err) {
            return done(err)
          }

          if (!isValid) {
            return done(null, false)
          }

          return done(null, user)
        })
      })

    })
  }
))
/*
passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (user, done) {
  done(null, user);
});
*/

app.use('/', require('./router'));

/**
 * error handling
 */
app.use((err, req, res, next) => {
  console.log(err);
  res.status(500).send('Server Error');
});

/**
 * start express server
 */
app.listen(app.get('port'), () => {
  console.log('%s DatePoll-Backend running at %s:%s', chalk.green('✓'), app.get('host'), app.get('port'));
  console.log('  Press CTRL-C to stop\n');
})
