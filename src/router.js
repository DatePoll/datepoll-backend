const controller = require('./controller')
const api = require('./api')

const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()
const urlParser = bodyParser.urlencoded({
  extended: false
})

const router = require('express').Router()

router.use((req, res, next) => {
  next()
})

router.use('/', urlParser, controller)
router.use('/api', jsonParser, api);

module.exports = router
