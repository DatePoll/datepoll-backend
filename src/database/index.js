const mysql = require('mysql')

const DATABASE_TYPES = {
  mysql: 'MYSQL'
}

var enabled_DatabaseType = process.env.DATABASE_TYPE

var conn

var getDatabaseConnection = function () {
  switch (enabled_DatabaseType) {
    case DATABASE_TYPES.mysql:
      conn = mysql.createConnection({
        host: process.env.MYSQL_HOST,
        user: process.env.MYSQL_USER,
        password: process.env.MYSQL_PASSWORD,
        database: process.env.MYSQL_DATABASE
      });
      break
    default:
      throw new Error('DatabaseType \'' + enabled_DatabaseType + '\' is not implemented!')
  }

  conn.connect(function (err) {
    if (err) throw err;
  });
}

module.exports = {
  getDatabaseConnection: getDatabaseConnection,
  conn: conn
}
