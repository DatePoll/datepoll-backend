# noinspection SqlNoDataSourceInspectionForFile

DROP DATABASE IF EXISTS datepoll;

CREATE DATABASE datepoll
  DEFAULT CHARACTER SET utf8;
USE datepoll;

SET SESSION sql_mode = 'ALLOW_INVALID_DATES';

CREATE TABLE users (
  userID                INT          NOT NULL UNIQUE PRIMARY KEY AUTO_INCREMENT,
  -- Can be null because we have an extra table for verification
  password              VARCHAR(512),

  firstname             VARCHAR(128) NOT NULL,
  surname               VARCHAR(128) NOT NULL,
  email                 VARCHAR(128) NOT NULL UNIQUE,
  title                 VARCHAR(64),

  location              VARCHAR(128),
  streetname            VARCHAR(128),
  -- We use varchar because there are street numbers which contains letters
  streetnumber          VARCHAR(16),
  zip_code              INT,

  birthdate             DATE         NOT NULL,
  joindate              DATE         NOT NULL,
  active                TINYINT      NOT NULL                    DEFAULT 0,

  hasBeenActivated      TINYINT      NOT NULL                    DEFAULT 0,
  activationCode        INT          NOT NULL,
  confirmedEmailAddress TINYINT      NOT NULL                    DEFAULT 0,
  -- Notes about the user
  userNotes             VARCHAR(2000)                            DEFAULT NULL,
  adminNotes            VARCHAR(2000)                            DEFAULT NULL
);

-- Move new users into the default subgroup after inserted
CREATE TRIGGER T_ADD_TO_DEFAULT_SUBGROUP
  AFTER INSERT ON users FOR EACH ROW
  INSERT INTO user_is_member_of (subgroupID, userID)
  VALUES(
    (SELECT subgroupID FROM subgroups ORDER BY subgroupID ASC LIMIT 1),
    (SELECT userID FROM users WHERE email = NEW.email)
  );

-- add default-entry for each new user in the permission table
CREATE TRIGGER T_INIT_PERMISSIONS
  AFTER INSERT ON users FOR EACH ROW
  INSERT INTO permissions (userID, isAdmin, canManageUsers, canManagePolls, canManageOrganisation, canManageGroups, canManageSubgroups)
  VALUES ((SELECT userID FROM users WHERE email = NEW.email), 0, 0, 0, 0, 0, 0);

-- Users can have multiple telephone numbers
CREATE TABLE telephone_numbers (
  userID INT         NOT NULL,
  label  VARCHAR(32) NOT NULL,
  number VARCHAR(32) NOT NULL,

  FOREIGN KEY (userID) REFERENCES users (userID) ON DELETE CASCADE
);

CREATE TABLE tokens (
  userID            INT           NOT NULL,
  token             VARCHAR(512)  NOT NULL UNIQUE PRIMARY KEY,
  createdDate       DATE          NOT NULL,
  lastUsedTokenDate DATE          NOT NULL,
  browserName       VARCHAR(128),
  browserVersion    VARCHAR(128),
  userAgent         VARCHAR(256),
  operatingsystem   VARCHAR(128),

  FOREIGN KEY (userID) REFERENCES users (userID) ON DELETE CASCADE
);

CREATE TABLE organisation (
  name         VARCHAR(64) NOT NULL UNIQUE PRIMARY KEY,
  description  LONGTEXT,
  homepageLink VARCHAR(128),
  --  should be used in any texts on the webpage to provide the user with the current age of the organisation
  foundingDate DATE
);

CREATE TABLE groups (
  groupID     INT         NOT NULL UNIQUE PRIMARY KEY AUTO_INCREMENT,
  name        VARCHAR(128) NOT NULL UNIQUE,
  description LONGTEXT
);

CREATE TRIGGER T_CREATE_DEFAULTSUBGROUP
  AFTER INSERT
  ON groups
  FOR EACH ROW INSERT INTO subgroups (subgroupID, groupID, name)
VALUES (NULL, NEW.groupID, concat('Default-subgroup of ', NEW.name));

CREATE TABLE subgroups (
  subgroupID  INT         NOT NULL UNIQUE PRIMARY KEY AUTO_INCREMENT,
  name        VARCHAR(128) NOT NULL UNIQUE,
  groupID     INT         NOT NULL,
  description LONGTEXT,

  FOREIGN KEY (groupID) REFERENCES groups (groupID)
    ON DELETE CASCADE
);

CREATE TABLE user_is_member_of (
  subgroupID INT NOT NULL,
  userID     INT NOT NULL,

  FOREIGN KEY (subgroupID) REFERENCES subgroups (subgroupID),
  FOREIGN KEY (userID) REFERENCES users (userID)
    ON DELETE CASCADE
);

CREATE TABLE polls (
  pollID      INT         NOT NULL UNIQUE PRIMARY KEY AUTO_INCREMENT,
  title       VARCHAR(64) NOT NULL,
  description LONGTEXT,
  finishDate  DATETIME    NOT NULL
);

CREATE TABLE polls_for_subgroups (
  pollID      INT NOT NULL,
  subgroupID  INT NOT NULL,

  FOREIGN KEY (pollID) REFERENCES polls (pollID) ON DELETE CASCADE,
  FOREIGN KEY (subgroupID) REFERENCES subgroups (subgroupID) ON DELETE CASCADE
);

CREATE TABLE polls_for_groups (
  pollID  INT NOT NULL,
  groupID INT NOT NULL,

  FOREIGN KEY (pollID) REFERENCES polls (pollID) ON DELETE CASCADE,
  FOREIGN KEY (groupID) REFERENCES groups (groupID) ON DELETE CASCADE
);

CREATE TABLE decision (
  decisionID  INT         NOT NULL UNIQUE PRIMARY KEY AUTO_INCREMENT,
  name        VARCHAR(32) NOT NULL UNIQUE,
  description LONGTEXT,
  -- text which can be choosen on the page e.g. ✓, x, don't know yet, ...
  value       VARCHAR(18) NOT NULL
);

CREATE TABLE user_voted_for_poll (
  voteID     INT NOT NULL UNIQUE PRIMARY KEY AUTO_INCREMENT,
  pollID     INT NOT NULL,
  userID     INT NOT NULL,
  decisionID INT NOT NULL,

  FOREIGN KEY (pollID) REFERENCES polls (pollID) ON DELETE CASCADE,
  FOREIGN KEY (userID) REFERENCES users (userID) ON DELETE CASCADE,
  FOREIGN KEY (decisionID) REFERENCES decision (decisionID) ON DELETE CASCADE
);

CREATE TABLE permissions (
  userID                INT     NOT NULL UNIQUE,
  isAdmin               TINYINT NOT NULL DEFAULT 0,
  canManageUsers        TINYINT NOT NULL DEFAULT 0,
  canManagePolls        TINYINT NOT NULL DEFAULT 0,
  canManageOrganisation TINYINT NOT NULL DEFAULT 0,
  canManageGroups       TINYINT NOT NULL DEFAULT 0,
  canManageSubgroups    TINYINT NOT NULL DEFAULT 0,

  FOREIGN KEY (userID) REFERENCES users (userID)
    ON DELETE CASCADE
);

CREATE TABLE performance_badge (
  badgeID     INT         NOT NULL UNIQUE PRIMARY KEY AUTO_INCREMENT,
  name        VARCHAR(64) NOT NULL,
  description LONGTEXT
);

CREATE TABLE user_has_badge (
  badgeID    INT       NOT NULL,
  userID     INT       NOT NULL,
  subgroupID INT       NOT NULL,
  since      TIMESTAMP NOT NULL,

  FOREIGN KEY (badgeID) REFERENCES performance_badge (badgeID) ON DELETE CASCADE,
  FOREIGN KEY (userID) REFERENCES users (userID) ON DELETE CASCADE,
  FOREIGN KEY (subgroupID) REFERENCES subgroups (subgroupID) ON DELETE CASCADE
);


CREATE TABLE meeting (
  meetingID INT         NOT NULL UNIQUE PRIMARY KEY AUTO_INCREMENT,
  startDate TIMESTAMP   NOT NULL,
  endDate   TIMESTAMP   NOT NULL,
  name      VARCHAR(64) NOT NULL,
  note      LONGTEXT,
  forGroup  INT,

  FOREIGN KEY (forGroup) REFERENCES groups (groupID)
);

#init organisation
INSERT INTO organisation (name, description, homepageLink, foundingDate)
VALUES ('Default-Organisation', NULL, NULL, NULL);

#init decisions
INSERT INTO `decision` (`name`, `description`, `value`) VALUES ('Nein', 'Ich habe keine Zeit', '0');
INSERT INTO `decision` (`name`, `description`, `value`) VALUES ('Ja', 'Ich habe Zeit', '1');

#init default groups
INSERT INTO `groups` (groupID, name, description) VALUES (NULL, 'DEFAULT_GROUP', 'Default-group');