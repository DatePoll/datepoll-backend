# DatePoll-Backend
## Installation
- clone the repo onto your server in a folder where nodejs has full access to it
- install nodejs, npm
- call `npm update` in the command line in order to download all needed dependencies
- configure your DatePoll-instance with the `.env`-file in the `conf` directory

## Start
In order to start your DatePoll server, simply call `npm start` in the root directory of this repository. To check if it was reaalllly successful, go to e.g. http://localhost:8080. You should see a tiny status-message.<p>
That's it! The DatePoll-Applicationserver is up and running!